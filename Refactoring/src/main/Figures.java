package main;
public class Figures {
        private double radius, width,length,side_a,side_b,side_c, pi = Math.PI;;
	    
		public Figures(){
        	this.setRadius(1);
        	this.width=1;
        	this.length=1;
        	this.side_a=1;
        	this.side_b=1;
        	this.side_c=1;
        }
        
        public double circle_area() {
	        // A = π r^2
	        return pi * Math.pow(getRadius(), 2);
	    }
	    public double circle_perimeter() {
	        // P = 2πr
	        return 2 * pi * getRadius();
	    }

	    public double rectangle_area() {
	    	// A = w * l
	    	return width * length;
	    }

	    public double rectangle_perimeter() {
	    	// P = 2(w + l)
	    	return 2 * (width + length);
	    }
	    
	    public double triangle_area() {
	    	// Heron's formula:
	        // A = SquareRoot(s * (s - a) * (s - b) * (s - c)) 
	        // where s = (a + b + c) / 2, or 1/2 of the parameter of the triangle 
	        double s = (side_a + side_b + side_c) / 2;
	        return Math.sqrt(s * (s - side_a) * (s - side_b) * (s - side_c));
	    }
	    
	    public double triangle_perimeter() {
	    	// P = a + b + c
	        return side_a + side_b + side_c;
	    }
	    
	    public static void main(String[] args) {
	    	Figures f=new Figures();
	    	// Rectangle test
	        double width = 5, length = 7;
	        System.out.println("Rectangle width: " + width + " and length: " + length
	                + "\nResulting area: " + f.rectangle_area()
	                + "\nResulting perimeter: " + f.rectangle_perimeter() + "\n");

	        // Circle test
	        double radius = 5;
	        System.out.println("Circle radius: " + radius
	            + "\nResulting Area: " + f.circle_area()
	            + "\nResulting Perimeter: " + f.circle_perimeter() + "\n");

	        // Triangle test
	        double a = 5, b = 3, c = 4;
	        System.out.println("Triangle sides lengths: " + a + ", " + b + ", " + c
	                + "\nResulting Area: " + f.triangle_area()
	                + "\nResulting Perimeter: " + f.triangle_perimeter() + "\n");
	    }

		public double getRadius() {
			return radius;
		}

		public void setRadius(double radius) {
			this.radius = radius;
		}

}
	
