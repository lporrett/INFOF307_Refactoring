package test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import main.Figures;

public class FiguresTest {
	
	private Figures f;
	private double result;
	private double roundOff;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.f = new Figures();
		result=0.0;
		roundOff=0.0;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFigures() {
		assert f.getRadius()==1;
	}

	@Test
	public void testCircle_area() {
		f.setRadius(4);
		result = f.circle_area();
		roundOff = Math.round(result * 100.0) / 100.0;
		assert roundOff==50.27;
	}

	@Test
	public void testCircle_perimeter() {
		f.setRadius(4);
		result = f.circle_perimeter();
		roundOff = Math.round(result * 100.0) / 100.0;
		assert roundOff==25.13;		
	}

	@Test
	public void testRectangle_area() {
		result = f.rectangle_area();
		assert result==1;
	}

	@Test
	public void testRectangle_perimeter() {
		result = f.rectangle_perimeter();
		assert result==4.0;
	}

	@Test
	public void testTriangle_area() {
		result = f.triangle_area();
		roundOff = Math.round(result * 100.0) / 100.0;
		assert roundOff==0.43;
	}

	@Test
	public void testTriangle_perimeter() {
		result = f.triangle_perimeter();	
		assert result==3.0;
	}

}
